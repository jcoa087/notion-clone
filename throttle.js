

export const throttle = (cb,delay = 150)=>{
    let wait = false

    return(...args)=>{
        
        if(wait) return 

        wait = true

        setTimeout(() => {
            cb(...args)
            wait = false
        }, delay);
    }
}