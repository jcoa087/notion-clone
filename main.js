import { throttle } from "./throttle.js" 

/*Bindings*/
const 
dragElements = document.querySelectorAll('.custom-list__list-element'),
workAreaList = document.querySelector('.work-area__custom-list'),
createListElement=()=>{
    const newListElement = document.createElement('li')
    newListElement.classList.add('custom-list__list-element')
    newListElement.draggable = true
    newListElement.innerHTML=`
    <input class="custom-list__input-text"/>
    <div class="custom-list__options">
        <i class="custom-list__add fa-solid fa-plus"></i>
        <img class="custom-list__dots" src="assets/grip-vertical.svg" alt="grip dots">
    </div>
    `

    return newListElement
}

let list=[],item={},value=''
/*Function declaration*/
function setHandlers (dragElements){
    dragElements.forEach(dragElement=>{
        dragElement.addEventListener('input',()=>{
            // console.log(dragElement.querySelector('.custom-list__input-text').value)
            list.push(dragElement.querySelector('.custom-list__input-text').value)
            console.log(list)
        })
        dragElement.addEventListener('dragstart',()=>{
            dragElement.classList.add('dragging')
        })
    
        dragElement.addEventListener('dragend',()=>{
            dragElement.classList.remove('dragging')
        })

        dragElement.addEventListener('touchstart',()=>{
            dragElement.classList.add('dragging')
        })
    
        dragElement.addEventListener('touchend',()=>{
            dragElement.classList.remove('dragging')
        })
    
    })
    
}

function adddElement(){

    
    workAreaList.appendChild(createListElement())
    const 
    dragElements = document.querySelectorAll('.custom-list__list-element'),
    elements = [...dragElements],
    currentElement = elements[elements.length - 1]
    
    setHandlers(dragElements)
    currentElement.querySelector('.custom-list__input-text').focus()
    
}

function getClosestElement(axisY,elements){

    return elements.reduce((closest,child)=>{
        const box = child.getBoundingClientRect()
        const offset = axisY - box.top - box.height/2
        
        if(offset<0 && offset>closest.offset) return closest = {offset,child}
        else return closest
    },{offset:Number.NEGATIVE_INFINITY})
}

function setWorkArea(workAreaList,e){

    const 
    elements = document.querySelectorAll('.custom-list__list-element:not(.dragging'),
    draggingElement = document.querySelector('.dragging'),
    nextElement = getClosestElement( e.clientY || e.touches[0].clientY,[...elements])


    if(nextElement.child==null && draggingElement!=null) workAreaList.appendChild(draggingElement)
    else if(draggingElement!=null) workAreaList.insertBefore(draggingElement,nextElement.child)
    
}

function workListEvents(){
    workAreaList.addEventListener('dragover',throttle((e)=>{
        e.preventDefault()
        console.log('work')
        setWorkArea(workAreaList,e)
    }))
    
    workAreaList.addEventListener('touchmove',throttle((e)=>{
        e.preventDefault()
        
        setWorkArea(workAreaList,e)
    
    }))
}

function addElementFunction(){
    
    document.addEventListener('click',(e)=>{
        if(!e.target.matches('.work-area__add-task'))return
        e.preventDefault()
        adddElement(workAreaList)
        
    })

    document.addEventListener('click',(e)=>{
        if(!e.target.matches('.custom-list__add'))return
        e.preventDefault()
        adddElement(workAreaList)
        
    })


}

function getValue(){}
/*Llamado de functions*/
addElementFunction()

workListEvents()

setHandlers(dragElements)